 

4.4.5 Groups

         As we have mentioned, each user belongs to one or more groups. The only real importance of group relationships pertains to file permissions, as you'll recall from Section 3.9, each file has a ``group ownership'' and a set of group permissions which defines how users in that group may access the file. 

There are several system-defined groups such as bin, mail, and sys. Users should not belong to any of these groups; they are used for system file permissions. Instead, users should belong to an individual group such as users. If you want to be cute, you can maintain several groups of users such as student, staff, and faculty. 

  The file /etc/group contains information about groups. The format of each line is 

group name:password:GID:other members 

Some example groups might be: 

root:*:0: 
users:*:100:mdw,larry 
guest:*:200: 
other:*:250:kiwi 

    The first group, root, is a special system group reserved for the root account. The next group, users, is for regular users. It has a GID of 100. The users mdw and larry are given access to this group. Remember that in /etc/passwd each user was given a default GID. However, users may belong to more than one group, by adding their usernames to other group lines in /etc/group. The groups command lists what groups you are given access to. 

The third group, guest, is for guest users, and other is for ``other'' users. The user kiwi is given access to this group as well. 

As you can see, the ``password'' field of /etc/group is rarely used. It is sometimes used to set a password on group access. This is seldom necessary. To protect users from changing into priveleged groups (with the newgroup command), set the password field to ``*''. 

        The commands addgroup or groupadd may be used to add groups to your system. Usually, it's easier just to add entries in /etc/group yourself, as no other configuration needs to be done to add a group. To delete a group, simply delete its entry in /etc/group. 


 
