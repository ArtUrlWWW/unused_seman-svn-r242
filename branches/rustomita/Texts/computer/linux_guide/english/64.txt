 

1.6.1 Hints for UNIX novices

 
Installing and using your own Linux system does not require a great deal of background in UNIX. In fact, many UNIX novices successfully install Linux on their systems. This is a worthwhile learning experience, but keep in mind that it can be very frustrating to some. If you're lucky, you will be able to install and start using your Linux system without any UNIX background. However, once you are ready to delve into the more complex tasks of running Linux---installing new software, recompiling the kernel, and so forth---having background knowledge in UNIX is going to be a necessity. 

Fortunately, by running your own Linux system you will be able to learn the essentials of UNIX necessary for these tasks. This book contains a good deal of information to help you get started---Chapter 3 is a tutorial covering UNIX basics, and Chapter 4 contains information on Linux system administration. You may wish to read these chapters before you attempt to install Linux at all---the information contained therein will prove to be invaluable should you run into problems. 

Nobody can expect to go from being a UNIX novice to a UNIX system administrator overnight. No implementation of UNIX is expected to run trouble- and maintenance-free. You must be aptly prepared for the journey which lies ahead. Otherwise, if you're new to UNIX, you may very well become overly frustrated with the system. 


 
