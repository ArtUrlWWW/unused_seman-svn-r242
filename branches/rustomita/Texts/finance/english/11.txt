                       The Administrative Department

       The main tasks and functions of this Department are:

          * providing organisational, technical and information
            services for the Bank of Russia�s Board of Directors,
            the National Banking Council and the Chairman of the
            Bank of Russia;
          * providing organisational and documentary services for
            the central apparatus;
          * dealing with international financial organisations,
            representative offices of the foreign banks accredited
            in Russia, national banks and banks with foreign
            capital;
          * doing protocol work and organising overseas trips for
            Bank of Russia employees;
          * conducting expert and legal analysis of Bank of Russia
            construction, material and technical support projects;
          * personnel management;
          * making administrative personnel cost estimates,
            compiling manning tables and doing the accounting;
          * providing social amenities for employees.
