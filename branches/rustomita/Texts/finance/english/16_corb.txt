 
	ECONOMIC AND POLITICAL NEWS  

The total amount of the international aid package that Russia will receive in 
1998 is $14.8 billion. This amount includes the $11.2 billion loan from 
International Monetary Fund with the interest rate of 4.6-4.8%. The World Bank 
will offer a 17-year loan of $1 billion at 5%. Russia will also receive a 17-
year $800 million loan from the Japan�s ExIm Bank at the annual rate of 2.5%.  

The first tranche of $5.6 billion of the IMF�s loan is expected to be received 
several days after the meeting of the IMF�s board of directors that will be held 
on July 20. This tranche will be used to supplement the foreign exchange 
reserves of the Central Bank. The second instalment of $3.5 billion will be 
released in early September. The Ministry of Finance will receive $2.8 billion 
from this instalment. The third tranche of $2.1 that is expected in November 
will be added to the Central Bank�s reserves.  

According to IMF deputy director Stanley Fischer, the first instalment of the 
IMF aid package will be released only if the principal measures of the Russian 
government�s stabilisation programme receive legislative support.  

Russia�s State Duma has approved in three readings 14 legislation bills proposed 
by the government within the frameworks of the stabilisation programme.  

On July 16, the State Duma approved on third and final reading the main part of 
the new Tax Code. According to another bill approved by the deputies, the main 
part of the new Tax Code, with the exception of the articles listing the 
particular taxes, will be put in force January 1, 1999.  

The deputies approved amendments to the Law on Profits Tax that clarify the way 
the taxable profit is calculated if products are sold below cost. According to 
the law, losses from sales of securities are deducted from the taxable profit. 
The law stipulates lowering of profit tax for organisations and industrial 
companies from 35% to 30% and increasing it for banks and credit organisations 
to 45%.  

The Sate Duma approved the bill on sales tax stipulating a 5% sales tax on all 
goods with the exception of bread, milk and goods for children.  

The Duma approved a number of legislation bills that are not included in the 
stabilisation programme. Among them is the bill on foreign investment in Russia 
that strengthens the legal infrastructure for foreign investment. Another bill 
approved by the Duma stipulates reduction of the existing average excise rate on 
oil from 55 roubles to 25 roubles per tonne.  

Addressing the State Duma deputies on July 15, the Russia�s Finance Minister 
Mikhail Zadornov said that according to the government�s stabilisation program, 
federal budget expenditures in 1999 would be about 300 billion roubles. The 
budget revenues will grow by between 50 and 60 billion roubles as a result of 
more effective operation of customs and tax services, introduction of imputed 
tax on small businesses and a number of other measures.  

President Boris Yeltsin signed the law that increases tax on purchasing foreign 
currency from 0.5% to 1%.  

Russia�s foreign exchange and gold reserves fell from $15.1 billion to $13.5 
billion over the period from July 4 to July 14.  

According to estimates of IMF experts, Russia�s budget deficit will decline to 
5.6% of GDP in 1998 from 6.8% of GDP in 1997.  
  
	GOVERNMENT SECURITIES MARKET  

The Russian government decided to issue at least $2 billion in new Eurobonds by 
July 20 in exchange for GKOs maturing before July 1, 1999. The 7- and 20-year 
dollar denominated bonds will be sold at a significant discount to par at a 
modified Dutch auction based on investors� bids. If the total amount of bids is 
less than $2 billion, the exchange will not take place. The bids must be 
submitted by July 17 and the results of the auction will be announced on July 
20. The swap is aimed at easing Russia�s crushing debt servicing burden that has 
contributed to the financial crisis. The government also decided not to issue 
new GKOs.  

On Tuesday, July 14, yields on all GKO issues fell significantly, since after 
the positive developments in securing the international rescue package for 
Russia the fears of rouble devaluation subsided and GKOs became attractive for 
investors once again. By the end of the trading day Tuesday, the yields of GKOs 
with medium and long maturities were in the range of 57-62%, down from 100-120% 
a day earlier. There was no GKO placement auction Wednesday but only regular 
secondary trading. The volume of funds in the MICEX GKO trading system reached 
7.2 billion roubles, a record for the last several months. By the end of the 
week, GKO yields fell to 50-57% for long maturities, 40-50% for medium 
maturities and 20-40% for short maturities.  
  
	COMPANIES  

 Oil & gas  

According to the Prime Minister Sergei Kiriyenko, the government plans to create 
one or several gas transporting companies in the structure of Gazprom.  

The board of directors of Gazprom announced that the company would limit 
supplies of gas to customers who do not pay for it. The decisions of stopping 
gas supplies to insolvent customers will be taken after approval of respective 
local authorities. The board of directors has also decided to cut capital 
expenditures on investment projects. According to First Deputy Prime Minister 
Boris Nemtsov, Gazprom plans to introduce accounting of separate activities 
including extraction, transporting and marketing of gas.  

The Fuel and Energy Ministry will recommend the Ministry of Finance and the Tax 
Service to give Russia�s UES and Gazprom a grace period until August 15 for its 
tax payments. The two monopolies are not recommended to start sharp reduction of 
electricity and gas supplies until August 1.  

Within the program of better information disclosure, NK Surgutneftegaz signed a 
contract with DeGolyer&Mac Naughton of the U.S. to conduct an independent audit 
of the company�s oil and natural gas reserves. The audit is expected to be 
completed in late 1998 or early 1999. According to data of Surgutneftegaz, the 
company�s ABC1 reserves are 1.4 billion tonnes of oil and 500 billion cu.m. of 
natural gas.  

The President of NK Sibneft Andrei Blokh resigned from his position. The First 
Vice-President Evgeny Shvidler has been appointed the acting president until the 
next shareholders meeting.  

NK YUKOS has been completing modernisation of the company�s oil-processing 
capacities including Kuibyshev, Novokuibyshev and Syzran refineries. The 
modernisation has allowed YUKOS to stop production of ethylated gasoline, 
increase the share of high-octane fuels in the production and improve 
profitability. The total amount of capital investment in the project is $70 
million.  

The State Duma decided to put off the auction for a stake in NK Slavneft until 
Duma�s approval of the 1998 privatisation programme.  

Three Russian companies were included in the Forbes rating of 800 foreign 
companies ranked by the sales revenues. Gazprom, LUKoil, Surgutneftegaz and 
Mosenergo are listed 80th, 261th, 571th, and 630th, respectively. The top spot 
of the rating is occupied by Mitsui of Japan.  

 Energy   

Sverdlovenergo has been working with The Bank of New York to launch Global 
Depository Receipts (GDRs) on its shares. The Bank of New York will start 
converting the company�s shares into GDRs in early August 1998. According to 
preliminary estimates, the volume of GDR issue may total 10-12% of 
Sverdlovenergo�s charter capital or about 697 million roubles. Fleming UCB will 
serve as the financial consultant in the project and lead manager of the 
placement. According to a representative of Sverdlovenergo, the company will put 
off implementation of its Level-1 ADRs project because of unfavourable situation 
in the financial markets.  

Sverdlovenergo reduced consumption of gas by 50% as a consequence of Gazprom�s 
demands of cash payments for gas. Sverdlovenergo has paid in cash for only about 
6-7% of the supplied gas.  

 Telecommunications  

Sistema Financial Company and Interros financial-industrial group have been 
negotiating possible joint participation in the tender for the 25% minus two 
shares stake in Svyazinvest.  

Novgorodtelecom submitted the to the US SEC documentation required to register 
its Level-1 ADR programme. The company plans to issue ADRs on the stock 
currently help in the company�s treasury (8,731 common shares representing 6.4% 
of the company�s charter capital).  

 Other Companies  

In 1998, Perm Motors will receive external financing in the total amount of $45 
million. UNEXIM Bank offered a 4-year loan worth $15 million. Pratt & Whitney 
has contributed $15 million in the company�s charter capital and will provide 
another $15 million in the third quarter of 1998. The shareholders of Perm 
Motors are Perm Motors Holding that holds a 49% stake, Interros with a 26% stake 
and Pratt & Whitney (25%).  

Fitch IBCA cut the long-term credit rating of UNEXIM Bank from BB- to B+ with a 
stable outlook. According to the agency�s data, financial condition of the bank 
has worsened as a result of financial crisis in Russia.  

On July 9, Norilsk Nickel submitted to SEC the documentation required to 
register the company�s Level-1 ADR programme.  
  
	 
  
		The RTS Index increased by 34.3% on the week. The trading volume for 
the week totalled $ 286 mln.  

On Wednesday July 14, RTS trading was halted for 15 minutes because the RTS 
index rose by 16.84%. On same day, equities trading was halted several times at 
MICEX and at MSE.  
.
   
		
   
