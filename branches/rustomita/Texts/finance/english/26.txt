                                 STATEMENT

   by the National Banking Council under the Bank of Russia on the Draft
      Guidelines for the Government�s Uniform Monetary Policy for 1997

  (Passed by the National Banking Council under the Bank of Russia at its
                       meeting on November 27, 1996)

1. The Draft Guidelines for the Government�s Uniform Monetary Policy for
1997 are worked out by the Bank of Russia in compliance with the
requirements of the Federal Law on the Central Bank of the Russian
Federation (Bank of Russia).

2. The Draft Guidelines for the Government�s Uniform Monetary Policy for
1997 are based on the macroeconomic indicators of the national development
forecast, taking into account the calculations made for the draft federal
law on the Federal Budget for 1997, on the objectives of the stabilisation
and strengthening of the national currency and the creation of conditions
conducive to economic growth. The draft sets the necessary conditions for
the realisation of the official macroeconomic forecast for 1997.

3. Guided by the expediency to maintain continuity in the attainment of the
principal objective of ensuring stability of the national currency, the
Bank of Russia sets the monetary policy targets for 1997. A low rate of
inflation will be possible despite a significant growth in the money supply
in real terms (inflation will not exceed 12% a year, while the money supply
will grow by 22%-30% p.a.).

4. The projected foreign exchange policy corresponds to the proclaimed
monetary policy and, by scaling down inflationary expectations, it
contributes to the stabilisation and predictability of economic activity.

5. Although enhancing the credibility of the banking system is a separate
Bank of Russia function, independent from the monetary policy, in the draft
Guidelines for the Government�s Uniform Monetary Policy for 1997 the Bank
of Russia devoted special attention to the state of the banking system
(because its stability is a necessary condition for more effective
implementation of monetary policy tasks).

Approved at the meeting of the National Banking Council under the Bank of
Russia on April 24, 1997 Protocol No. 3, Part 1
