                  The Accounting and Reporting Department

       The main tasks of the Accounting and Reporting Department are
       to elaborate the methodological principles of organising and
       compiling reports and accounts in roubles and foreign
       currencies, set uniform accounting rules for banking
       operations in the Bank of Russia�s institutions and credit
       organisations in Russia and ensure the provision of full and
       authentic accounting information about the activities of the
       Bank of Russia, its property and financial position. To
       accomplish the tasks assigned to it, the Department fulfils
       the following functions:

          * elaborates and sets out accounting rules and standards
            for the Bank of Russia and credit organisations;
          * compiles the Chart of Accounts and sets the rules for
            its use in the Central Bank and credit organisations,
            oversees the inclusion of amendments, additions and
            changes in them and informs the Russian banking
            community and the corresponding Bank of Russia units
            about the changes made;
          * determines the accounting and presentation procedures
            for entering specific banking operations in the banks'
            balance sheets and works on the problem of harmonising
            their accounting procedures with international norms;
          * establishes the procedure by which the regional branches
            of the Bank of Russia and other organisations
            accountable to the Bank of Russia which are part of the
            Bank of Russia system must compile and submit regular
            (current) and annual reports and accounts;

       Jointly with other Bank of Russia units this department:

          * participates in drafting similar regulatory instructions
            for credit organisations;
          * elaborates regulatory instructions on accounting and
            reporting methodology for the enterprises and
            organisations accountable to the Bank of Russia and for
            the credit organisations;
          * compiles the Bank of Russia�s consolidated balance sheet
            on a monthly basis for its publication in the press and
            the Bank of Russia�s consolidated annual accounting
            report, which is subsequently included in the annual
            report the Bank of Russia submits to the State Duma;
          * examines and analyses the annual reports of the
            subsidiaries of foreign banks and Russian banks in which
            the Bank of Russia holds foreign exchange valuables;
          * drafts instructions, orders, provisions and other Bank
            of Russia regulations on questions related to banks�
            accounting and reporting;
          * cooperates with the corresponding international economic
            organisations, banking associations and credit
            organisations on issues related to accounting and
            reporting;
          * studies foreign experience in accounting and reporting
            and uses it in its practices;
          * organises and conducts seminars and meetings on
            accounting and reporting.
