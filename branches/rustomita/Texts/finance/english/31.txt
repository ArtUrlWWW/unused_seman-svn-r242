

        5-Rouble Banknote

        The 5-rouble banknote is designed on the basis of
        the 5,000-rouble banknote of the 1995 issue. It is
        137 mm by 61 mm and it is made on paper of a
        light-green hue, containing violet, red and
        light-green fibres. The predominant colour of the
        banknote is green. The paper has local watermarks on
        the left and right coupon fields. On the broad
        coupon field is St. Sofia's Cathedral in Novgorod
        and on the narrow coupon field is the figure 5. The
        paper contains a vertical security thread, which
        becomes visible when the banknote is held to the
        light. The thread bears the recurrent, direct and
        reverse inscription "[Image]", printed with a black
        paint.

        On the obverse side of the banknote is the dark-blue
        Bank of Russia logo, the figure 5 printed in a
        dark-green colour and the embossed inscription "
        [Image]" (Bank of Russia Note), printed in a
        dark-green colour, which changes into dark-blue on
        the word "[Image]". In the centre is an engraved
        picture of the monument built in Novgorod to
        commemorate Russia's millenary; the left part of the
        monument is dark-blue and the right one is
        dark-green; in the background is St. Sofia's
        Cathedral in Novgorod. Below is the figure 5,
        printed with a silvery paint, the legend
        "Counterfeiting Bank of Russia Notes Is an
        Indictable Crime", embossed signs for people with
        poor sight, the inscription "[Image]" (Five
        Roubles), and a stylised ribbon, whose left part is
        dark-blue and right one is dark-green. The ribbon
        bears two Cyrillic letters PP, which can only be
        seen when one scrutinises the banknote under the
        oblique incident light (kipp-effect). In the lower
        part of the right coupon field is a grey-coloured
        ornamental rosette with the light-coloured
        denomination 5 in its left and right parts and the
        green-coloured figure 5 in the centre. On the left
        side of the central part of the note is a
        multi-coloured ornamental vertical band. In the
        centre of the narrow left-hand coupon field is the
        code and serial numbers of the banknote, printed in
        a red-brown colour; in the upper part of the wide
        right-hand coupon field are the code and serial
        number of the note, printed in a dark-green colour.
        The code consists of two letters and the serial
        number consists of seven digits.

        In the upper left- and right-hand corners of the
        reverse side of the banknote are the figures 5 in
        ornamental rosettes printed in a green colour. In
        the centre is the engraved image of the fortress
        wall of the Novgorod Kremlin; on the left under the
        engraving is the inscription "Novgorod" in an
        ornamental frame, further are six dark-blue double
        horizontal bands bearing the recurrent microprinted
        motto "[Image]" and a large figure 5, printed in a
        dark-green colour. Below is the lettered
        denomination "[Image]" (Five Roubles), printed in
        dark-green and dark-blue colours, the blue-coloured
        issue year 1997. On the right in the central part is
        a multi-coloured ornamental vertical band, whose
        blue elements match the white elements of the
        obverse side and form a single pattern with them
        when the note is held to the light.

        10-Rouble Banknote

        The 10-rouble banknote is designed on the basis of
        the Bank of Russia 10,000-rouble note of the 1995
        issue. The banknote is 150 mm by 65 mm and the paper
        is of light-yellow hue and contains violet, red and
        light-green fibres. The predominant colours of the
        banknote are dark-green and dark-brown. The paper
        has local watermarks on the left-hand and right-hand
        coupon fields. On the wide coupon field is a chapel
        in the city of Krasnoyarsk and on the narrow coupon
        field is the figure 10. The paper contains a
        vertical security thread, which is visible when the
        note is held to the light. The thread bears the
        recurrent direct and reverse motto,
        printed in a black colour.

        On the obverse side of the banknote are: above - the
        Bank of Russia logo printed in brown, the figure 10
        printed in dark-green and the embossed legend Bank 
        of Russia Note). In the centre is an
        engraving showing a bridge across the River Yenisei
        in Krasnoyarsk and a chapel on a multi-coloured
        background. Below is the number 10 printed with a
        silvery paint, the inscription "Counterfeiting Bank
        of Russia Notes Is an Indictable Crime", embossed
        signs for people with poor sight and a stylised
        ribbon. The ribbon bears the motto "Krasnoyarsk",
        printed in a frame and two Cyrillic letters PP,
        which can only be seen when the note is scrutinised
        under oblique incident light (kipp-effect). To the
        right of the ribbon is the lettered denomination "
        Ten Roubles). In the lower part of the
        right coupon field is a grey-coloured ornamental
        rosette with the light-coloured denomination 10 in
        its left and right parts and the blue- coloured
        figure 10 in the centre. In the right part of the
        note is a multi-coloured ornamental vertical band
        containing stylised fir-tree cones and twigs. In the
        centre of the narrow left-hand coupon field are the
        code and serial number of the banknote, printed in a
        red-brown colour, and in the upper part of the wide
        right-hand coupon field are the code and serial
        number in dark green. The code consists of two
        letters and the serial number has seven digits.

        In the left-hand and right-hand upper corners of the
        reverse side of the banknote are the figures 10 in
        dark-green patterned rosettes. In the centre is an
        engraving of the dam of the Krasnoyarsk Hydro Power
        Plant, with the lettered denomination below
        Ten Roubles); above the denomination are
        seven microprinted horizontal double bands composed
        of the recurrent figure 10, and on the right is the
        light-coloured micropinted motto on nine
        dark-green horizontal bands, on the right is a large
        figure 10, printed in a dark-green colour and the
        issue year 1997, printed in a light-brown colour. On
        the left in the central part is a vertical
        ornamental band composed of stylised fir-tree cones
        and twigs, whose untinged elements are filled with
        the colour of the corresponding elements of the band
        on the obverse side when the note is held to the
        light.

        50-Rouble Banknote

        The 50-rouble banknote is designed on the basis of
        the Bank of Russia 50,000-rouble note of the 1995
        issue. It is a 150 mm by 65 mm piece of light-blue
        paper, containing violet, red and light-green
        fibres. Its predominant colour is blue. The paper
        has local watermarks in the left and right coupon
        fields. On the wide coupon field is the Cathedral of
        Sts. Peter and Paul in St. Petersburg and on the
        narrow coupon field is the figure 50. A vertical
        security thread, which becomes visible when the note
        is held to the light, carries the recurrent, direct
        and reverse motto "[Image]", printed in black.

        In the upper part of the obverse side of the
        banknote is the Bank of Russia logo and the figure
        50, printed with a dark-blue paint, and the embossed
        inscription Bank of Russia Note). In the
        centre is an engraved picture of a sculpture at the
        foot of the Rostral Column in St. Petersburg on the
        River Neva embankment and in the background is a
        view of the Fortress of Sts. Peter and Paul. Below
        is the figure 50, printed with a silvery paint, the
        inscription "Counterfeiting Bank of Russia Notes Is
        an Indictable Crime", embossed signs for people with
        poor sight, the denomination in words 
        (Fifty Roubles) and a stylised ribbon. The ribbon
        contains the Cyrillic letters PP, which become
        visible when the note is scrutinised under the
        oblique incident light (kipp-effect). In the lower
        part of the right-hand coupon field is an ornamental
        grey-coloured rosette with the light-coloured figure
        50 to the left and to the right and another figure
        50, printed a green-blue colour, in the centre. In
        the left part of the note is a vertical
        multi-coloured ornamental band. In the centre of the
        narrow left-hand coupon field are the code and
        serial number of the banknote, printed in a
        red-brown colour, and in the upper part of the wide
        right-hand coupon field are the code and serial
        number, printed in dark-green. The code consists of
        two letters and the serial number is composed of
        seven digits.

        In the upper left-hand and right-hand corners of the
        reverse side of the banknote is the figure 50 in
        patterned rosettes and microprinted blue horizontal
        lines made of the figure 50. In the centre is an
        engraved picture of the former stock exchange and
        the Rostral Column in St. Petersburg. Under the
        engraving is a decorative shield bearing the
        inscription "Sankt-Peterburg" and to the right are
        several dark horizontal bands with the recurrent
        light-coloured microprinted motto and a
        large figure 50. Underneath is the denomination in
        words Fifty Roubles), printed in blue and
        black and the issue year 1997, printed in a violet
        colour. On the right in the central part is a
        vertical multi-coloured ornament, containing rhombs,
        whose untinged elements are filled with pink colour
        when the note is held to the light.

        100-Rouble Banknote

        The 100-rouble note is designed on the basis of the
        Bank of Russia 100,000-rouble note of the 1995
        issue. It is a 150 mm by 65 mm piece of
        pink-coloured paper, containing violet, red and
        light-green fibres. The predominant colour of the
        banknote is brown-green. The paper has local
        watermarks in the left-hand and right-hand coupon
        fields. In the wide coupon field is the building of
        the Bolshoi Theatre in Moscow and in the narrow
        coupon filed is the figure 100. A vertical security
        thread, which becomes visible when the note is held
        to the light, carries the recurrent, direct and
        reverse motto, printed in black.

        In the upper part of the obverse side of the
        banknote is the Bank of Russia logo and the figure
        100, printed with a brown paint, and the embossed
        inscription (Bank of Russia Note). In the
        centre is an engraved sculptural group (quadriga) on
        the portico of the Bolshoi Theatre building in
        Moscow. Below is the figure 100, printed with a
        silvery paint, the inscription "Counterfeiting Bank
        of Russia Notes Is an Indictable Crime", embossed
        signs for people with poor sight, a stylised ribbon
        and the denomination in words (One Hundred
        Roubles). The ribbon contains the Cyrillic letters
        PP, which become visible when the note is
        scrutinised under the oblique incident light
        (kipp-effect). In the lower part of the right-hand
        coupon field is an ornamental grey-coloured rosette
        with the light-coloured figure 100 to the left and
        to the right and another figure 100, printed with a
        dark-orange paint, in the centre. In the left part
        of the note is a vertical multi-coloured ornamental
        band. In the centre of the narrow left-hand coupon
        field are the code and serial number of the
        banknote, printed in a red-brown colour, and in the
        upper part of the wide right-hand coupon field are
        the code and serial number, printed in dark-green.
        The code consists of two letters and the serial
        number is composed of seven digits.

        In the upper left-hand and right-hand corners of the
        reverse side of the banknote are the figures 100 in
        patterned rosettes and between them are microprinted
        red-brown horizontal lines made of the figure 100.
        In the centre is an engraved picture of the Bolshoi
        Theatre building in Moscow. Under the engraving is a
        decorative shield bearing the inscription "Moskva",
        five red-brown double horizontal lines with the
        recurrent light-coloured microprinted motto
        and a large red-brown figure 100.
        Underneath to the left is the denomination in words
        (One Hundred Roubles) and to the right is
        the issue year 1997, printed in green. On the right
        in the central part is a vertical ornamental band,
        containing rhombs, whose untinged triangular
        elements are filled with light blue colour when the
        note is held to the light.

        500-Rouble Banknote

        The 500-rouble banknote is designed on the basis of
        the Bank of Russia 500,000-rouble note of the 1995
        issue. It is a 150 mm by 65 mm piece of paper of
        light-violet hue, containing violet, red and
        light-green fibres. The predominant colour of the
        banknote is violet-blue. The paper has local
        watermarks in the left-hand and right-hand coupon
        fields. In the wide coupon field is the head of the
        monument to Peter the Great and in the narrow coupon
        field is the figure 500. The paper contains a
        vertical security thread, which becomes visible when
        the note is held to the light, and the thread
        carries the recurrent, direct and reverse motto
        printed in black.

        In the upper part of the obverse side of the
        banknote is a Bank of Russia logo, printed with a
        paint that changes its colour from yellow-green to
        red-orange when the note is viewed from different
        angles, the figure 500, printed in a violet colour
        and the embossed inscription Bank of
        Russia Note). In the centre is an engraving showing
        the monument to Peter the Great in Arkhangelsk with
        a sailing ship and sea terminal in the background.
        Below is the figure 500, printed with a silvery
        paint, the inscription "Counterfeiting Bank of
        Russia Notes Is an Indictable Crime", signs for
        people with poor sight, a stylised ribbon with the
        motto "Arkhangelsk" and the denomination in words 
        Five Hundred Roubles). The ribbon contains
        the Cyrillic letters PP, which become visible when
        the note is scrutinised under the oblique incident
        light (kipp-effect). On the left of central part of
        the note is a multi-coloured ornamental vertical
        band. In the lower part of the right-hand coupon
        field is an ornamental grey-coloured rosette with a
        light-coloured figure 500 to the left and to the
        right and another figure 500, painted crimson, in
        the centre. In the centre of the narrow left-hand
        coupon field are the code and serial number of the
        banknote, printed in red, and in the upper part of
        the wide right-hand coupon field are the code and
        serial number printed in dark-green. The code
        consists of two letters and the serial number is
        composed of seven digits.

        In the upper left-hand and right-hand corners of the
        reverse side of the banknote is the figure 500 in
        patterned rosettes and between them are microprinted
        violet-coloured bands made of the figure 500. In the
        centre is an engraved picture of the Solovetsky
        Monastery. Underneath is a large figure 500, the
        denomination in words(Five Hundred
        Roubles), four horizontal violet-coloured double
        lines with the recurrent light-coloured microprinted
        motto and the issue year 1997, printed
        with a crimson paint. On the right in the central
        part is a vertical ornamental band, whose untinged
        elements are filled with the colour of the
        corresponding elements of the ornamental band on the
        obverse side of the note when it is held to the
        light.


