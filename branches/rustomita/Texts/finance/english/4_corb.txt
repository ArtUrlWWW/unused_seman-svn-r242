 
  
	ECONOMIC AND POLITICAL NEWS  
  
On April 24, the State Duma, Russia�s lower house of parliament, voted to 
approve Sergei Kiriyenko as prime minister. 25 deputies voted against Kiriyenko 
and 251 for.  

On April 21, the Chicago Mercantile Exchange started trading in Russian rouble 
futures and options contracts. The rouble futures contract will have profits and 
losses denominated in US dollars, be cash-settled and sized at 500,000 roubles 
(about $81,400).  
  
	COMPANIES  

 Oil & gas  

LUKoil was included into Corporation Records informational guide published by 
Standard & Poor�s. From now on, LUKoil will have �blue sky� in some US states 
meaning that it will have an opportunity to widen its ADRs market in the US. 
Corporation Records guide contains financial and production data on more than 
1,200 US and international corporations.  

LUKoil plans to acquire an oil refinery in Czech republic. LUKoil supplies about 
70% of Czech crude oil imports and has several gas stations in this country.  

According to Alexander Braverman, First Deputy Property Minister, the government 
may choose to sell shares in Rosneft oil company in smaller stakes if the tender 
for 75% plus one share stake planned for the end of May fails to attract 
bidders.  

Tyumen Oil Company (TNK) plans to distribute high-yield bonds in the US in the 
second half of 1998. It is expected that the yield will be about 12-13%. TNK 
plans to spend about $900 mln on capital investment projects in 1998. $500 mln 
will come from a loan offered by US ExIm Bank. The company plans to raise the 
remaining $400 mln through bond issues and loans from Western banks.  
Gazprom intends to issue convertible bonds worth about $1 billion by July 1998. 
The terms of the issue have not yet been determined.  

 Energy   

Russia�s Federation Council approved a law that limits the share of foreign 
investors in Russia�s UES to 25% and secures a 51% stake in state property.  

Inkombank and Krasnoyarskenergo signed an agreement on cooperation in the 
international financial markets. The agreement includes obtaining an 
international credit rating, issue of ADRs and Eurobonds, and arranging of 
syndicated loans.  

 Telecommunications  

Petersburg Telephone Network will not proceed with its preferred share issue if 
the issue raises less than $40 million.  
Dmitry Vasiliev, executive director of the Federal Securities Commission, 
supported the controversial share issue by MGTS. According to the original 
privatisation terms of MGTS, the issue should be sold at a nominal price to 
Moscow Committee for Science and Technology (MCST) that currently holds 25% of 
the company�s shares.  

Svyazinvest gave instructions to start preparing for the merger of the telecom 
companies in St. Petersburg region. The merger will probably have several 
stages. First, Lensvyaz will merge with Telegraph, then PTS will merge with MMT, 
and then a unified company will be formed. The merger process will probably be 
preceded by the companies� shareholders meetings, at which both common and 
preferred shareholders will vote, and the decision will need to be approved by a 
75% vote.  

By the end of May, Svyazinvest will prepare a list of telecoms that will be 
allowed to place new share issues. In June, the company plans to attract a $400 
mln syndicated loan that will be used to finance capital investment at regional 
telecoms.  

Novosibirskelectrosvyaz is going to issue level-1 ADRs on 5% of its shares in 
September or October 1998.  

Martelecom intends to attract about $14 mln to finance its capital investment 
projects for 1998-2000.  

Chelyabinsksvyazinform intends to place ADRs on its shares by the end of the 
year. The amount of the issue will be announced after the shareholders meeting.  

 Other Companies  

Uralmash Plants plan to issue convertible bonds. In October or November 1998, 
the company�s ADRs will be listed on the London Stock Exchange and later on 
NYSE. 

Sales of Uralmash Plants and Izhorskie Plants will total $500 mln after the 
merger. It is planned to increase the sales of the holding to about $1 billion 
in several years. Red Sormovo and Volgograd Drilling Equipment Plant have also 
received offers to join the holding. The merger process will be completed by the 
mid-summer. 

By the middle of summer, Nizhnekamskneftekhim expects to receive the results of 
the IAS audit of its financial statements for the last three years conducted by 
Price Waterhouse. Nizhnekamskneftekhim continues working on its level-1 ADR 
programme. 
  
	 
