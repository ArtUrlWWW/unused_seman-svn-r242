SITUATION AND TRENDS IN THE REAL SECTOR 
The Russian economy is in the state of depression. The trend to production 
recession in the real sector is initiated by reduction in aggregate demand. 
Since 4th quarter 1998, dynamics of major elements of final consumption of GDP 
have found themselves under a substantial impact on the part of the ongoing 
reduction in households' final consumption and investment demand. The retail 
trade volume which is always an indicator of changes in households' expenditures 
dropped by over 15% when compared with the period of time between January to 
April 1998. Despite some improvement of indices of economic agents' financial 
performance, one notes a remaining trend to reduction in investment in capital 
assets and construction work.
The national industry rather promptly reacted to the change in the domestic 
situation. Given that the output of goods and services of the basic industry 
branches drooped by 3.7% compared with the Ist quarter 1998, the volume of 
industrial production fell by 2%. Since late 1998, due to a sharp Rb. 
depreciation one has observed positive monthly dynamics of industrial output. A 
comparative analysis of results of the survey on business activity by various 
sources testifies to a trend to growth in the entrepreneurial optimism index. 
The industrial sector has become a factor inhibiting the negative trends' effect 
in the real sector.

Between January- April 1998, the volume of industrial output reduced by 0.5% 
when compared with its respective period of the prior year. One notes a gradual 
reduction in recession intensity in the fuel and energy, consumer and ferrous 
metallurgical sectors. According to results of the first four months 1999 
production increment made up: chemicals- 8.0%, non- ferrous metallurgy- 6.6%, 
forestry- 4.8%, food- processing- 3.1%.
Since the beginning of this year the state of affairs has found itself under a 
positive impact of lowering inflationary expectations. The slowdown of inflation 
pace is accompanied by changes in price ratios. By the results of 1998, USD 
appreciated against Rb. as much as 3.5 times, while CPI grew by 84%, industrial 
producers' price index- by 23%. As a result of depreciation of Rb., financial 
and economic indices of different industry branches differentiated depending on 
their performance in external markets. That created a non- homogeneous potential 
in the real sector.
Since February 1999, the industrial producers' price rise rate has become 
superior to the dynamics of consumer prices. In all, between January to April 
1999 price index in industry and CPI made up 119.5% and 121.3%, respectively. It 
is export-oriented industry branches, primarily non- ferrous metallurgy, 
forestry and pulp and paper the price rise rate for products of which exceeds an 
average one by the national industry as a whole- 159.3% and 131.3%, 
respectively. Due to lowering competition on the part of import analogues, one 
observes a growing gap between price indices of producers in the light ( 124.9%) 
and food- processing ( 131.4%) sectors and CPIs which keeps inflationary 
potential in the consumer market.
Those domestic enterprises which had restructured their production prior to the 
crisis took an advantage of the changed situation and expand their business in 
the domestic market by ousting inefficient competitors. In addition to that, the 
industrial enterprises' reactivating takes place on the background of a 
weakening competition on the part of import goods because of the Rb. 
depreciation to USD. One notes an intensive development of import- substituting 
processes, primarily in machine building, industry of construction materials, 
food - processing sector. Since early 1999, in order to keep their positions in 
the domestic market, integration processes have showed themselves between 
enterprises in mining and manufacturing sectors. Relatively stable situation in 
the real sector allows to conclude that a certain potential has been accumulated 
in the economy, and the scope of that is likely to be underestimated to a 
significant extent. That may be attributed to both specific conditions of the 
national economy's development under the hypertrophied emerging of non- monetary 
system of settlements and barter transactions as well as inadequate statistical 
observations over the ongoing processes.
The analysis of the situation in the national economy and generalization of 
results of entrepreneurs' projections does not give any grounds for pessimistic 
forecasts of the Russian economy's development this year. One however should 
consider the fact that the present situation is specific because the emerged 
trends are not stable while regulating factors are highly mobile. That provides 
for a thorough systemic analysis of decision making and evaluation of its 
consequences.
O. Izryadnova
Top
 
