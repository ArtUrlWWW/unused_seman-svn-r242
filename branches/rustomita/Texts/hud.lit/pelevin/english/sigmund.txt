Viktor Pelevin
Sigmund in a Cafe

                        Sometimes hidden behind the smooth stone faces of these 
                        idols are labyrinths of cracks and hollows, inhabited by 
                        various kinds of birds. 
                        Joseph Lavender, "Easter Island"
He didn't remember such a cold winter in Vienna yet. Every time the door opened 
and a cloud of cold air flew into the cafe, he shivered a little. For a long 
time no new visitors came, and Sigmund fell into a light senile nap, but now the 
door banged again, and he raised his head to look. 
Two newcomers just entered the cafe - a whiskered gentleman and a lady with a 
high chignon. 
The lady held a long sharp umbrella in her hands. 
The gentleman carried a small purse decorated by dark shiny furs, a little moist 
from the melted snowflakes. 
They stopped at the hanger and began undressing: the man took off his overcoat, 
hung it on a peg, and then tried to hang his hat on one of the long wooden knobs 
that jutted out of the wall above the hanger, but missed, and the hat fell out 
of his hand and down on the floor. The man muttered something, lifted his hat 
and hung it finally on the knob; then he hurried to help her take off her 
furcoat. Relieved of the furcoat, the lady smiled benevolently and took her 
purse from him, but suddenly she grimaced in distress: the lock on the purse had 
been open, and some snow had fallen in. She hanged the purse on her shoulder, 
put the umbrella into the corner with its handle down for some reason, took her 
companion's hand and went with him into the main room. 
- Aha, - said Sigmund softly and shook his head. 
Between the wall and the bar counter, near the table where the whiskered 
gentleman and his companion went, was a small empty space where the barkeeper's 
children were playing: a boy of about eight in a bulky white sweater covered 
with black diamonds, and a girl still younger, in a dark dress and striped 
woolen pants. Their playthings, wooden bricks and a half-deflated ball, lay 
beside them on the floor. 
The kids were unusually quiet. The boy was ocuppied with a pile of wooden bricks 
with painted sides, he was building a house of a somewhat strange shape, with an 
opening in the front wall; the house would collapse all the time, because the 
opening was too wide and the upper brick fell in between the sides. Every time 
the bricks ended up strewn around the floor, the boy would sadly pick his nose 
with a dirty finger and then start building anew. The girl sat in front of her 
brother right on the floor, watching him without much interest and playing with 
a handful of small change - she would lay the coins out on the floor, or gather 
them in a small pile and shove it under herself. Soon she was bored with it, she 
dropped the coins, leaned aside, grabbed the nearest chair by its legs, pulled 
it to her and started moving it around on the floor and pushing the ball with 
the chair's legs. Once she pushed too hard, the ball rolled toward the boy, and 
his feeble construction collapsed at the very moment when he was going to mount 
on top of it the last brick, the sides of which showed a branch of an orange 
tree and a firepost. The boy lifted his head and shook his fist at her, to which 
she opened her mouth and stuck out her tongue - she held it out for so long that 
one could perhaps examine it in all detail. 
- Aha, - said Sigmund and looked at the whiskered man and the lady. 
They already had the appetizers served to them. The gentleman was swallowing the 
oysters, knowingly opening their shells with a small silver knife, and was 
telling something to his companion, who was smiling, nodding and eating 
mushrooms - she would take them with a two-toothed fork one by one from the 
plate and scrutinize them before dipping in a thick yellow sauce. The gentleman, 
clinking with the bottle on the brim of his glass, poured himself some white 
wine, drank it and moved the soup bowl closer to himself. 
The waiter brought a plate with a long fried fish. The lady looked at the fish 
and suddenly smacked herself on the forehead and started telling something to 
the gentleman. He looked at her, listened to her for a while and grimaced 
doubtfully, then drank another glass of wine and started carefully putting a 
cigarette into a conical red cigarette holder, which he held between his little 
finger and his ring finger. 
-Aha! - said Sigmund and stared at the far corner of the room, where the 
hostess, the barkeeper's wife, stood with a stocky waiter. 
It was dark there, or rather it was darker than in other corners, because the 
lightbulb under the ceiling has burnt out. The hostess was staring up with her 
plump hands on her hips; because of her pose and her apron with colorful zigzags 
she resembled an ancient vase. The waiter has already fetched a long folding 
ladder, which stood now beside an empty table. The hostess checked that the 
ladder was sturdy enough, scratched her head ponderously and said something to 
the waiter. He turned and went around the bar counter, then stooped behind it 
and was not seen for a while. After a minute, he emerged from behind the counter 
and showed to the hostess an elongated, shining object. She nodded 
energetically, and the waiter came back to her with the found flashlight in his 
raised hand. He wanted to give it to her, but she shook her head and pointed her 
finger to the floor. 
There was a large square hatch in the floor beside the empty table. It was 
almost invisible because its lid was covered with the same parquet diamonds as 
the rest of the floor, and one could suspect its existence only from the double 
border line of thin copper which crossed the intricate parquet patterns, and 
from the inlaid copper ring. 
The waiter meticulously pulled his pants at the knees, squatted, grabbed the 
ring and with one powerful tug opened the hatch. The hostess grimaced and 
shifted. The waiter looked at her questioningly, but she energetically nodded 
again, and he started climbing down. Apparently, there was a ladder beneath the 
floor, as he sank into the blackness of the square in short jerks, one for each 
invisible step. At first he held the lid himself, but when he was too deep down, 
the hostess helped him by leaning forward and grasping the lid with her two 
hands, and staring intently into the dark hole where the waiter went. 
After a while the waiter's white coat, rather dirty from cobwebs and dust, 
appeared again above the floor. He got out, resolutely closed the hatch and 
moved to the ladder, but the hostess stopped him and turned him around. She 
thoroughly dusted his coat, took the lightbulb from his hand, breathed on it and 
stroked it a few times with the palm of her hand. She moved to the ladder, put 
her foot on the lower step, waited until the waiter firmly held the ladder from 
aside, and started climbing up. 
The burnt lightbulb was fitted inside a narrow glass lampshade which hung on a 
long string, so she didn't have to climb too high. She went five or six steps 
up, reached with her hand inside the lampshade and tried to turn the bulb, but 
it was screwed in too tightly, and the lampshade started turning with it. Then 
the hostess took the new bulb into her mouth, cautiously holding it with her 
lips, lifted her other hand and held the lampshade by its rim; this way it went 
much easier. She unscrewed the burnt bulb, put it into a pocket in her apron, 
and started fitting in the new one. The waiter's attention was riveted to the 
movements of her plump palms, as he was holding the ladder in his strong hands 
and moistening his lips with the tip of his tongue. Suddenly the light broke out 
of the matted lampshade, the waiter shuddered, blinked and loosened his grip for 
a moment. The sides of the folding ladder started to come apart; the hostess 
waved her hands and almost fell on the floor, but the waiter managed to hold the 
ladder at the last moment; with incredible speed the hostess, pale from fright, 
made it over the three or four steps to the parquet floor and stood weak and 
motionless in the calming embrace of her companion. 
- Aha! Aha! - Sigmund said aloud and stared at the couple at the table. 
The lady with the chignon was already eating dessert: in her hand was an oblong 
tube with cream, and she nibbed at it from the wider side. When Sigmund looked 
at her, she was just about to take a larger bite: she put the tube in her mouth 
and pressed with her teeth, but the thick white cream broke through the thin 
gilded box at its rear end. The whiskered gentleman reacted instantly, and the 
ejected protuberance of cream fell into his hand instead of slumping on the 
table-cloth. The lady broke out laughing. The gentleman brought his hand with a 
pile of cream to his mouth and licked it off, which made his companion laugh 
even more, so that she even gave up her cookie and dropped it on the plate with 
the remains of the fish. After licking the cream, the gentleman caught the 
lady's hand and gave it a heart-felt kiss, to which she took his glass of golden 
wine and took a few small sips. Thereafter, the gentleman lighted another 
cigarette: he put it into his conical red cigarette-holder, quickly inhaled a 
few times, and started to blow smoke rings. 
Without doubt, he was a master of that complicated art. At first he blew out one 
large blue-grey ring with a wavy brim, then another, smaller ring, which went 
through the first one without touching it. He waved his hand in the air, 
destroyed the smoke construction and made two new rings, now of the same size, 
which hung one above the other in an almost perfect figure-eight. His companion 
looked at it with interest, perfunctorily picking at the fish head with a thin 
wooden stick. 
Having gotten a lungful of smoke once again, hte gentleman blew out two thin 
long spurts, one of which went through the upper ring and the other through the 
lower, where they touched and converged into a muddy blueish cloud. The lady 
applauded. 
- Aha! - exclaimed Sigmund, and the gentleman turned and eyed him curiously. 
Sigmund looked at the children again. It seemed that one of them fetched some 
new toys. Beside the bricks and the ball, they now had disheveled dolls and 
colored pieces of clay lying around them. The boy was still busy with the 
bricks, but now instead of a house he was building a long, low wall, upon which 
at regular intervals stood tin soldiers with long red hats. A few openings were 
left in the wall, each guarded by three soldiers - one outside and two inside. 
The wall was shaped as a semicircle, and at its center a carefully arranged 
podium of four bricks held the ball - which rested only on the bricks, not 
touching the floor. The girl was sitting with her back to her brother and 
absent-mindedly biting at the tail of a stuffed canary. 
- Aha! - shouted Sigmund restlessly. - Aha! Aha! 
Now not only the whiskered gentleman glanced at him (the gentleman and the lady 
were already standing at the hanger and dressing up), but also the hostess, who 
was adjusting the window shades with a long stick. Sigmund looked at the hostess 
and then at the wall, which held a few paintings - a banal marine view with the 
moon and a beacon, and a huge, out-of-place avant-guarde painting, showing from 
above two open grand pianos, in which lay the dead Bounuel and Salvador Dali, 
both with strangely long ears. 
- Aha! - shouted Sigmund with all might. - Aha! Aha!! Aha!!! 
Now people from all sides looked at him, and not just looked: the hostess was 
approaching him with a long stick in her hand, and on the other side - the 
whiskered gentleman, holding his hat. The hostess frowned as usual, but the 
gentleman's face expressed a touching, genuine interest. The faces were getting 
closer until they occupied almost his entire view, and Sigmund felt ill at ease 
and cringed into a fluffy bundle. 
- What a beautiful parrot you have here, - the whiskered gentleman said to the 
hostess. - What else can he say? 
- Many things, - answered the hostess. - Come on, Sigmund, tell us something. 
She raised her hand and put the tip of a fat finger between the rods. 
- Nice boy Sigmund, - Sigmund said flirtingly, moving however along the rod to 
the far corner of the cage, just in case. - Clever boy Sigmund. 
- Clever boy he is, - the hostess said, - but the cage is full of his shit. Not 
a clean spot left. 
- Don't be too strict with the poor bird. It's his cage after all, not yours, - 
the whiskered gentleman said, preening his hair. - He has to live in it, too. 
A moment later he apparently felt embarassed about his talking to a vulgar 
barkeeper's wife. With a stiff face he put on his hat, turned and went to the 
door.

 



 
